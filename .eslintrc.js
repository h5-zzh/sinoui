module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:vue/base",
    "plugin:vue/essential",
    "plugin:vue/strongly-recommended",
    "plugin:vue/recommended"
  ],
  "parserOptions": {
    "ecmaVersion": 10,
    "sourceType": "module"
  },
  "plugins": [
    "vue"
  ],
  "rules": {
    "vue/valid-v-pre":0,
    "vue/valid-v-once":0,
    "vue/no-boolean-default":0,
    "vue/attributes-order":0,
  }
};
