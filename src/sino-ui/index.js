import SinoGrid from "./grid/grid";
import SinoInput from "./input/input";
import SinoText from "./input/text";
import SinoLabel from "./label/label";
import SinoCheck from "./select/check";
// import SinoDrop from "./select/drop";
import SinoButton from "./button/button";
import SinoDatatable from "./table/datatable";
import SinoContainer from "./container/container";
import SinoCard from "./card/card";
import SinoTab from './tab/tab';
import SinoTabPage from './tab/tabpage';
import SinoForm from "./form/form";
import SinoFormItem from "./form/formitem"
import SinoDatePicker from "./datepicker/datepicker";
import SinoSwitch from "./switch/switch";
import SinoDialog from "./dialog/dialog";
const SinoUI = {};
SinoUI.components = [
  SinoGrid,
  SinoInput,
  SinoText,
  SinoLabel,
  SinoCheck,
  // SinoDrop,
  SinoButton,
  SinoDatatable,
  SinoContainer,
  SinoCard,
  SinoTab,
  SinoTabPage,
  SinoForm,
  SinoFormItem,
  SinoDatePicker,
  SinoSwitch,
  SinoDialog
];

SinoUI.install = function(Vue) {
  this.components.map((r) => Vue.component(r.name, r));
};
export default SinoUI;
