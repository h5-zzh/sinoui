import Vue from "vue";
import App from "./App.vue";
import SinoUI from "./sino-ui/index";

Vue.config.productionTip = false;
Vue.use(SinoUI);

new Vue({
  SinoUI,
  render: (h) => h(App),
}).$mount("#app");
